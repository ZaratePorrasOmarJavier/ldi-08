
public class COnvertidor extends javax.swing.JFrame {

   
    public COnvertidor() {
        initComponents();
        origen.setLineWrap(true);
        destino.setLineWrap(true);
        origen.setWrapStyleWord(true);
        destino.setWrapStyleWord(true);
    }

   
    @SuppressWarnings("unchecked")
                           
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        origen = new javax.swing.JTextArea();
        btnBinText = new javax.swing.JButton();
        BtnTextBin = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        destino = new javax.swing.JTextArea();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        origen.setColumns(20);
        origen.setRows(5);
        jScrollPane1.setViewportView(origen);

        btnBinText.setText("Binario a Texto");
        btnBinText.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBinTextActionPerformed(evt);
            }
        });

        BtnTextBin.setText("Texto a Binario");
        BtnTextBin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BtnTextBinActionPerformed(evt);
            }
        });

        destino.setColumns(20);
        destino.setRows(5);
        jScrollPane2.setViewportView(destino);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("INGRESE TEXTO O CODIGO BINARIO");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("RESULTADO");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                    .addComponent(jScrollPane1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BtnTextBin)
                    .addComponent(btnBinText))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 321, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(160, 160, 160)
                        .addComponent(btnBinText, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(55, 55, 55)
                        .addComponent(BtnTextBin, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(186, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2)
                            .addComponent(jScrollPane1))
                        .addContainerGap())))
        );

        pack();
    }                        

    private void BtnTextBinActionPerformed(java.awt.event.ActionEvent evt) {                                           
        String texto = origen.getText();

        String codigoBinario = "";
        for (int i = 0; i < texto.length(); i++) {
            char letra = texto.charAt(i);
            int aux = Binario((int) (letra)).length();
            for (int j = 0; j < 8; j++) {
                if (aux < 8) {
                    codigoBinario = codigoBinario + "0";
                    aux++;
                }
            }
          
            codigoBinario = codigoBinario + Binario((int) (letra)) + " ";
        }
        destino.setText(codigoBinario);
    }                                          

    private void btnBinTextActionPerformed(java.awt.event.ActionEvent evt) {                                           
        
        String palabra = origen.getText().trim().replaceAll(" ", "");
        destino.setText(separaPorDigito(palabra));
    }                                          

    private String separaPorDigito(String digitoBinario) {
        String frase = "";

        for (int i = 0; i < digitoBinario.length(); i += 8) {
           
            String cadenaSeparada = digitoBinario.substring(i, i + 8);
           
            int decimal = Integer.parseInt(cadenaSeparada, 2);
           
            frase = frase + (char) decimal;
        }
        return frase;
    }

    private String Binario(int Decimal) {
        int R, x = 0;
        String Binario = ""; 
        R = Decimal % 2; 
        if (R == 1) {
            while (Decimal > 1) {
                Decimal /= 2;
                x = Decimal % 2;
                Binario = String.valueOf(x + Binario);
            }
        } else {
            while (Decimal > 0) {
                Decimal /= 2;
                x = Decimal % 2;
                Binario = String.valueOf(x + Binario);
            }
        }
        return String.valueOf(Binario + x);
    }

   
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ventana.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ventana().setVisible(true);
            }
        });
    }
                  
    private javax.swing.JButton BtnTextBin;
    private javax.swing.JButton btnBinText;
    private javax.swing.JTextArea destino;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea origen;
                     
}